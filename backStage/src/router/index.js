import Vue from 'vue'
import Router from 'vue-router'
import home from '@/components/home/index';
import client from '@/components/client/client';
import opportunity from '@/components/opportunity/opportunity';
import ventured from '@/components/ventured/ventured';
import followUp from '@/components/followUp/followUp';
import tender from '@/components/tender/tender';
import teamwork from '@/components/teamwork/teamwork';
import Administration from '@/components/Administration/Administration';
import myCenter from '@/components/myCenter/myCenter';
Vue.use(Router)

export default new Router({
  routes: [
    {
      path:'*',
      redirect:'/index'
    },
    {
      path:'/index',
      name:'home',
      component:home
    },
    {
      path:'/client',
      name:'client',
      component:client
    },
    {
      path:'/opportunity',
      name:'opportunity',
      component:opportunity
    },
    {
      path:'/ventured',
      name:'ventured',
      component:ventured
    },
    {
      path:'/followUp',
      name:'followUp',
      component:followUp
    },
    {
      path:'/tender',
      name:'tender',
      component:tender
    },
    {
      path:'/teamwork',
      name:'teamwork',
      component:teamwork
    },
    {
      path:'/Administration',
      name:'Administration',
      component:Administration
    },
    {
      path:'/myCenter',
      name:'myCenter',
      component:myCenter
    },
  ]
})
